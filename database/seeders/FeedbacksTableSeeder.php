<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class FeedbacksTableSeeder extends Seeder
{
    public function run()
    {
        $feedbacks = [];
        $faker     = Faker::create('ru_RU');
        $users     = $this->getIdAllUsers();

        for ($i = 1; $i <= 15; $i++) {
            $feedbacks[] = [
                'id'          => $faker->uuid,
                'user_id'     => Arr::random($users),
                'ip'        => $faker->ipv4,
                'name'        => $faker->name,
                'email'       => $faker->email,
                'message'     => $faker->text($maxNbChars = 500),
                'processed'   => Arr::random([true, false]),
                'admin_notes' => $faker->text($maxNbChars = 500),
                'updated_at'  => now()->addMinutes($i),
                'created_at'  => now()->addMinutes($i),
            ];
        }

        DB::table('feedback')->insert($feedbacks);
    }

    public function getIdAllUsers() : array
    {
        return DB::table('users')->get()->pluck('id')->toArray();
    }
}

