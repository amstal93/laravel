<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Services\Banner;

use App\Exceptions\IncorectSaveDataException;
use App\Http\Resources\BannerResource;
use App\Models\Banner;
use ArgumentCountError;
use Exception;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BannerService
{

    /**
     * @throws IncorectSaveDataException
     */
    public function createBanner($data) : Response
    {

        try {
            Banner::query()->create($data->toArray());
            return response(['message' => 'banner created'], Response::HTTP_CREATED);
        } catch (Exception | ArgumentCountError $e) {
            Log::error('CREATE BANNER ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updateBanner($data) : Response
    {
        try {
            Banner::query()->where('id', $data->id)->update($data->toArray());
            return response(['message' => 'banner updated'], Response::HTTP_OK);
        } catch (Exception | ArgumentCountError $e) {
            Log::error('UPDATE BANNER ' . $e->getMessage());
            throw new IncorectSaveDataException();
        }
    }

    public function getBannerId(string $id) : BannerResource
    {
        return Cache::remember($id, config('cache.lifetime'), function () use ($id) {
            return new BannerResource(Banner::findOrFail($id));
        });
    }

    public function getAllBanners(array $data) : ResourceCollection
    {
        return Cache::remember('banners_' . $data["page"], config('cache.lifetime'), function () use ($data) {
            return BannerResource::collection(Banner::desc()->paginate($data["per_page"]));

        });
    }
}
