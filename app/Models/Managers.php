<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property mixed password
 * @property mixed email_verified_at
 * @property mixed blocked
 * @method static paginate(mixed $per_page)
 * @method static findOrFail($id)
 */

class Managers extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, UuidTrait;

    /**
     * At least 1 Upper case letter, lower case letter, digit, Special symbols - 8 characters
     */
    public const PASSWORD_REGEX = '/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/';

    protected $fillable = [
        'id',
        'email',
        'permission',
        'name',
        'description',
        'blocked',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected     $casts = [
        'email_verified_at' => 'datetime:Y-m-d H:i:s',
        'created_at'        => 'datetime:Y-m-d H:i:s',
        'updated_at'        => 'datetime:Y-m-d H:i:s',
    ];

    public function getJWTIdentifier(): string
    {
        return $this->id;
    }

    public function getJWTCustomClaims(): array
    {
        return [];
    }

}
