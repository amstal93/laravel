<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

class Currencies extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'sign',
        'code',
        'rate_from',
        'rate_to',
        'cents',
        'position',
        'enabled'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        //
    ];
}
