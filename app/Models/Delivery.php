<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Models;

class Delivery extends BaseModel
{
    protected $fillable = [
        'id',
        'name',
        'description',
        'free_from',
        'price',
        'enabled',
        'position',
        'separate_payment',
        'image'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        //
    ];
}
