<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Exceptions;

use Exception;

class EmailUnconfirmedException extends Exception
{
    protected $message = 'email_not_confirmed';
}
