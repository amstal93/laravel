<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JWTAuth;

class AuthJWT
{
    public function handle(Request $request, Closure $next)
    {
        if (!JWTAuth::getToken()) return response(['error' => 'token_expired'], Response::HTTP_UNAUTHORIZED);
        return $next($request);
    }
}
