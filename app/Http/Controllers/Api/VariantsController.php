<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Variants;

class VariantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreVariantsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVariantsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Variants  $variants
     * @return \Illuminate\Http\Response
     */
    public function show(Variants $variants)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateVariantsRequest  $request
     * @param  \App\Models\Variants  $variants
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVariantsRequest $request, Variants $variants)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Variants  $variants
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variants $variants)
    {
        //
    }
}
