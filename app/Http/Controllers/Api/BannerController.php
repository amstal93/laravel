<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Banners\CreateBannerRequest;
use App\Http\Requests\Banners\GetAllBannersRequest;
use App\Http\Requests\Banners\UpdateBannerRequest;
use App\Http\Resources\BannerResource;
use App\Services\Banner\BannerService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Exceptions\IncorectSaveDataException;

class BannerController extends Controller
{
    private BannerService $service;

    /**
     * BannerController constructor.
     */
    public function __construct(BannerService $bannerService)
    {
        $this->service = $bannerService;
    }

    public function getAllBanners(GetAllBannersRequest $request): AnonymousResourceCollection
    {
        return $this->service->getAllBanners($request->toArray());
    }

    public function getBannerId($id): BannerResource
    {
        return $this->service->getBannerId($id);
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function createBanner(CreateBannerRequest $request): Response
    {
        return $this->service->createBanner($request);
    }

    /**
     * @throws IncorectSaveDataException
     */
    public function updateBanner(UpdateBannerRequest $request): Response
    {
        return $this->service->updateBanner($request);
    }
}
