<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Requests\BannerImages;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBannerImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'          => ['required', 'string', 'exists:banner_image,id'],
            'banner_id'   => ['required', 'string', 'exists:banners,id'],
            'name'        => 'required|string',
            'alt'         => 'required|string',
            'title'       => 'required|string',
            'description' => 'required|string',
            'link'        => 'required|string',
            'image'       => 'required|mimes:jpg,png,jpeg|max:118048',
            'position'    => 'required|integer',
            'visible'     => 'required|boolean',
        ];
    }
}
