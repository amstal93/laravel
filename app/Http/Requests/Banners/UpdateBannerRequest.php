<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace App\Http\Requests\Banners;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'          => ['required', 'string', 'exists:banners,id'],
            'page'        => ['required', 'string', 'min:3', 'max:50'],
            'name'        => 'required|string',
            'description' => 'required|string',
            'position'    => 'required|integer',
            'visible'     => 'required|boolean',
        ];
    }
}
