<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

namespace Tests\Feature\Api;

use App\Models\Binance\UserBinanceData;
use App\Models\Managers;
use App\Models\User;
use App\Traits\RefreshDatabase;
use Tests\TestCase;

class PrivateApiTest extends TestCase
{
    use RefreshDatabase;

    private $token;

    public function setUp(): void
    {
        parent::setUp();

        $params = [
            'email'    => 'admin@admin.com',
            'password' => '!123456Qwerty',
            'captcha'  => '00000000',
            'remember' => true,
        ];

        $response = $this->actingAs(Managers::first())
            ->post('/api/managers/login', $params);

        $responseObj = json_decode($response->getContent());

        $this->token = $responseObj->token;
    }

    public function tearDown(): void
    {
        parent::tearDown();
        unset($response, $this->token);
    }
    public function test_true_login(): void
    {
        $params = [
            'email'    => 'admin@admin.com',
            'password' => '!123456Qwerty',
            'captcha'  => '00000000',
            'remember' => true,
        ];

        $this->post( '/api/managers/login', $params)->assertOk();
    }

    public function testFakeLogin(): void
    {
        $params = [
            'email'    => 'admin@admin.com',
            'password' => '!123456Qwerts',
            'captcha'  => '00000000',
            'remember' => true,
        ];

        $this->postJson(config('test.host') . '/api/managers/login', $params)->assertForbidden();
    }

    public function test_get_login(): void
    {
        $this->get(config('test.host') . '/api/managers/login')->assertStatus(401);;
    }

    public function test_get_all_manager()
    {
        $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('get', config('test.host') . '/api/managers/all?page=1&per_page=5')
            ->assertStatus(200);
    }

    public function test_get_logout()
    {
        $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('get', config('test.host') . '/api/managers/logout')
            ->assertStatus(200);
    }
    public function test_post_logout()
    {
        $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('post', config('test.host') . '/api/managers/logout')
            ->assertStatus(405);
    }
}
