<?php
/*
 * Copyright © 2021
 * Author: Sergey Sobol
 * GitLab:https://gitlab.com/sobbol
 */

use App\Http\Controllers\Api\ManagersController;
use Illuminate\Support\Facades\Route;

Route::post('/login', [ManagersController::class, 'login']);

Route::middleware('auth.jwt')->group(function () {

    Route::post('/create', [ManagersController::class, 'createManager']);
    Route::get('/logout', [ManagersController::class, 'logout']);

    Route::get('/all', [ManagersController::class, 'getAllManagers']);


    Route::get('/{id}', [ManagersController::class, 'getManagerId']);
});
